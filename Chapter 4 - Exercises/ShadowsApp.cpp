//***************************************************************************************
// BoxDemo.cpp by Frank Luna (C) 2011 All Rights Reserved.
//
// Demonstrates rendering a colored box.
//
// Controls:
//		Hold the left mouse button down and move the mouse to rotate.
//      Hold the right mouse button down to zoom in and out.
//
//***************************************************************************************

#include"stdafx.h"
#include "d3dApp.h"
#include "d3dx11Effect.h"
#include "MathHelper.h"
#include "ShadowMap.h"

struct Vertex
{
	XMFLOAT3 Pos;
	XMFLOAT3 Normal;
};

struct Sphere {
	XMFLOAT3 Center;
	float Radius;
};

class ShadowsApp : public D3DApp
{
public:
	ShadowsApp(HINSTANCE hInstance);
	~ShadowsApp();

	bool Init();
	void OnResize();
	void UpdateScene(float dt);
	void DrawScene(); 

	void OnMouseDown(WPARAM btnState, int x, int y);
	void OnMouseUp(WPARAM btnState, int x, int y);
	void OnMouseMove(WPARAM btnState, int x, int y);

private:
	void BuildGeometryBuffers();
	void BuildBoxGeometryBuffer();
	void BuildPlaneGeometryBuffer();

	void BuildFX();
	void BuildFXShadow();
	void BuildVertexLayout();

	void DrawSceneToShadowMap();

	void BuildShadowTransform();

private:
	static const int SMapSize = 2500;
	ShadowMap* mShadowMap;

	Sphere mSceneBounds;

	ID3D11Buffer* mBoxVB;
	ID3D11Buffer* mBoxIB;

	ID3D11Buffer* mPlaneVB;
	ID3D11Buffer* mPlaneIB;

	DirectionalLight mDirLights[1];

	ID3DX11Effect* mFX;
	ID3DX11EffectTechnique* mTech;
	ID3DX11EffectMatrixVariable* mfxWorld;
	ID3DX11EffectMatrixVariable* mfxWorldViewProj;
	ID3DX11EffectMatrixVariable* mfxWorldInvTranspose;
	ID3DX11EffectMatrixVariable* mfxShadowTransform;
	ID3DX11EffectVectorVariable* mfxEyePos;
	ID3DX11EffectVariable* mfxDirLights;
	ID3DX11EffectVariable* mfxMaterial;
	ID3DX11EffectShaderResourceVariable* mfxShadowMap;

	ID3DX11Effect* mFXShadow;
	ID3DX11EffectTechnique* mfxShadowTech;
	ID3DX11EffectMatrixVariable* mfxShadowWorldViewProj;
	ID3DX11EffectScalarVariable* mfxShadowHeightScale;

	XMFLOAT4X4 mLightView;
	XMFLOAT4X4 mLightProj;
	XMFLOAT4X4 mShadowTransform;

	ID3D11InputLayout* mInputLayout;

	XMFLOAT4X4 mBoxWorld;
	XMFLOAT4X4 mPlaneWorld;
	XMFLOAT4X4 mView;
	XMFLOAT4X4 mProj;

	XMFLOAT3 mEyePos;

	Material mBoxMaterial;
	Material mPlaneMaterial;

	float mTheta;
	float mPhi;
	float mRadius;

	POINT mLastMousePos;
};

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE prevInstance,
				   PSTR cmdLine, int showCmd)
{
	// Enable run-time memory check for debug builds.
#if defined(DEBUG) | defined(_DEBUG)
	_CrtSetDbgFlag( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
#endif

	ShadowsApp theApp(hInstance);
	
	if( !theApp.Init() )
		return 0;
	
	return theApp.Run();
}
 

ShadowsApp::ShadowsApp(HINSTANCE hInstance)
: D3DApp(hInstance), mBoxVB(0), mBoxIB(0), mPlaneVB(0), mPlaneIB(0), mFX(0), mTech(0), mfxWorld(0), mfxShadowTransform(0),
  mfxWorldViewProj(0), mfxWorldInvTranspose(0), mfxEyePos(0), mfxDirLights(0), mfxMaterial(0), mfxShadowMap(0),
  mFXShadow(0), mfxShadowTech(0), mfxShadowWorldViewProj(0), mfxShadowHeightScale(0),
  mInputLayout(0), mTheta(1.5f*MathHelper::Pi), mPhi(0.25f*MathHelper::Pi), mRadius(5.0f), 
  mShadowMap(0)
{
	mMainWndCaption = L"Shadows Demo";
	
	mLastMousePos.x = 0;
	mLastMousePos.y = 0;

	XMMATRIX I = XMMatrixIdentity();
	XMStoreFloat4x4(&mBoxWorld, I);
	XMStoreFloat4x4(&mPlaneWorld, XMMatrixScaling(10, 1, 10) * XMMatrixTranslation(0, -1, 0));
	XMStoreFloat4x4(&mView, I);
	XMStoreFloat4x4(&mProj, I);

	mDirLights[0].AmbientFactor = 0.2f;
	mDirLights[0].Color = XMFLOAT4(0.7f, 0.7f, 0.7f, 1.0f);
	mDirLights[0].Direction = XMFLOAT3(-0.4f, -0.6f, 0.2f);

	mBoxMaterial.Diffuse = XMFLOAT4(1, 0, 0, 1);
	mBoxMaterial.Specular = XMFLOAT4(1, 1, 1, 2);

	mPlaneMaterial.Diffuse = XMFLOAT4(0, 0.75f, 0, 1);
	mPlaneMaterial.Specular = XMFLOAT4(0.3f, 0.3f, 0.3f, 2.0f);

	mSceneBounds.Center = XMFLOAT3(0, 0, 0);
	mSceneBounds.Radius = 15;
}

ShadowsApp::~ShadowsApp()
{
	SafeDelete(mShadowMap);

	ReleaseCOM(mBoxVB);
	ReleaseCOM(mBoxIB);
	ReleaseCOM(mPlaneVB);
	ReleaseCOM(mPlaneIB);
	ReleaseCOM(mFX);
	ReleaseCOM(mFXShadow);
	ReleaseCOM(mInputLayout);
}

bool ShadowsApp::Init()
{
	if(!D3DApp::Init())
		return false;

	BuildGeometryBuffers();
	BuildFX();
	BuildFXShadow();
	BuildVertexLayout();

	mShadowMap = new ShadowMap(md3dDevice, SMapSize, SMapSize);
	mfxShadowMap->SetResource(mShadowMap->DepthMapSRV());

	return true;
}

void ShadowsApp::OnResize()
{
	D3DApp::OnResize();

	// The window resized, so update the aspect ratio and recompute the projection matrix.
	XMMATRIX P = XMMatrixPerspectiveFovLH(0.25f*MathHelper::Pi, AspectRatio(), 1.0f, 1000.0f);
	XMStoreFloat4x4(&mProj, P);
}

void ShadowsApp::UpdateScene(float dt)
{
	// Convert Spherical to Cartesian coordinates.
	float x = mRadius*sinf(mPhi)*cosf(mTheta);
	float z = mRadius*sinf(mPhi)*sinf(mTheta);
	float y = mRadius*cosf(mPhi);

	// Build the view matrix.
	XMVECTOR pos    = XMVectorSet(x, y, z, 1.0f);
	XMVECTOR target = XMVectorZero();
	XMVECTOR up     = XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);

	XMMATRIX V = XMMatrixLookAtLH(pos, target, up);
	XMStoreFloat4x4(&mView, V);
	XMStoreFloat3(&mEyePos, pos);
}

void ShadowsApp::DrawScene()
{
	DrawSceneToShadowMap();

	md3dImmediateContext->ClearRenderTargetView(mRenderTargetView, reinterpret_cast<const float*>(&Colors::LightSteelBlue));
	md3dImmediateContext->ClearDepthStencilView(mDepthStencilView, D3D11_CLEAR_DEPTH|D3D11_CLEAR_STENCIL, 1.0f, 0);

	md3dImmediateContext->IASetInputLayout(mInputLayout);
    md3dImmediateContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	mfxEyePos->SetRawValue(&mEyePos, 0, sizeof(XMFLOAT3));
	mfxDirLights->SetRawValue(mDirLights, 0, 3 * sizeof(DirectionalLight));

	UINT stride = sizeof(Vertex);
    UINT offset = 0;

	// Draw box

    md3dImmediateContext->IASetVertexBuffers(0, 1, &mBoxVB, &stride, &offset);
	md3dImmediateContext->IASetIndexBuffer(mBoxIB, DXGI_FORMAT_R32_UINT, 0);

	XMMATRIX world = XMLoadFloat4x4(&mBoxWorld);
	XMMATRIX worldInvTrans = MathHelper::InverseTranspose(world);
	XMMATRIX view  = XMLoadFloat4x4(&mView);
	XMMATRIX proj  = XMLoadFloat4x4(&mProj);
	XMMATRIX worldViewProj = world*view*proj;
	mfxWorld->SetMatrix(reinterpret_cast<float*>(&world));
	mfxWorldViewProj->SetMatrix(reinterpret_cast<float*>(&worldViewProj));
	mfxWorldInvTranspose->SetMatrix(reinterpret_cast<float*>(&worldInvTrans));
	mfxMaterial->SetRawValue(&mBoxMaterial, 0, sizeof(Material));


	XMMATRIX shadowTransform = XMLoadFloat4x4(&mShadowTransform);
	mfxShadowTransform->SetMatrix(reinterpret_cast<const float*>(&(world * shadowTransform)));

    D3DX11_TECHNIQUE_DESC techDesc;
    mTech->GetDesc( &techDesc );
    for(UINT p = 0; p < techDesc.Passes; ++p)
    {
        mTech->GetPassByIndex(p)->Apply(0, md3dImmediateContext);
		md3dImmediateContext->DrawIndexed(36, 0, 0);
    }

	// Draw Plane

	md3dImmediateContext->IASetVertexBuffers(0, 1, &mPlaneVB, &stride, &offset);
	md3dImmediateContext->IASetIndexBuffer(mPlaneIB, DXGI_FORMAT_R32_UINT, 0);

	world = XMLoadFloat4x4(&mPlaneWorld);
	worldInvTrans = MathHelper::InverseTranspose(world);
	worldViewProj = world*view*proj;
	mfxWorld->SetMatrix(reinterpret_cast<float*>(&world));
	mfxWorldViewProj->SetMatrix(reinterpret_cast<float*>(&worldViewProj));
	mfxWorldInvTranspose->SetMatrix(reinterpret_cast<float*>(&worldInvTrans));
	mfxMaterial->SetRawValue(&mPlaneMaterial, 0, sizeof(Material));

	mfxShadowTransform->SetMatrix(reinterpret_cast<const float*>(&(world * shadowTransform)));

	mTech->GetDesc(&techDesc);
	for (UINT p = 0; p < techDesc.Passes; ++p)
	{
		mTech->GetPassByIndex(p)->Apply(0, md3dImmediateContext);
		md3dImmediateContext->DrawIndexed(6, 0, 0);
	}

	HR(mSwapChain->Present(0, 0));
}

void ShadowsApp::DrawSceneToShadowMap()
{
	mShadowMap->BindDsvAndSetNullRenderTarget(md3dImmediateContext);

	BuildShadowTransform();

	XMMATRIX view = XMLoadFloat4x4(&mLightView);
	XMMATRIX proj = XMLoadFloat4x4(&mLightProj);
	XMMATRIX viewProj = XMMatrixMultiply(view, proj);

	mfxShadowWorldViewProj->SetMatrix(reinterpret_cast<float*>(&viewProj));

	// These properties could be set per object if needed.
	mfxShadowHeightScale->SetFloat(0.07f);

	md3dImmediateContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	XMMATRIX world;
	XMMATRIX worldInvTranspose;
	XMMATRIX worldViewProj;

	UINT stride = sizeof(Vertex);
	UINT offset = 0;

	// draw the box into the shadow map

	md3dImmediateContext->IASetInputLayout(mInputLayout);
	md3dImmediateContext->IASetVertexBuffers(0, 1, &mBoxVB, &stride, &offset);
	md3dImmediateContext->IASetIndexBuffer(mBoxIB, DXGI_FORMAT_R32_UINT, 0);

	world = XMLoadFloat4x4(&mBoxWorld);
	worldViewProj = world*view*proj;

	mfxShadowWorldViewProj->SetMatrix(reinterpret_cast<float*>(&worldViewProj));

	D3DX11_TECHNIQUE_DESC techDesc;
	mfxShadowTech->GetDesc(&techDesc);
	for (UINT p = 0; p < techDesc.Passes; ++p)
	{
		mfxShadowTech->GetPassByIndex(p)->Apply(0, md3dImmediateContext);
		md3dImmediateContext->DrawIndexed(36, 0, 0);
	}

	// draw the plane into the shadow map

	md3dImmediateContext->IASetVertexBuffers(0, 1, &mPlaneVB, &stride, &offset);
	md3dImmediateContext->IASetIndexBuffer(mPlaneIB, DXGI_FORMAT_R32_UINT, 0);

	world = XMLoadFloat4x4(&mPlaneWorld);
	worldViewProj = world*view*proj;

	mfxShadowWorldViewProj->SetMatrix(reinterpret_cast<float*>(&worldViewProj));

	mfxShadowTech->GetDesc(&techDesc);
	for (UINT p = 0; p < techDesc.Passes; ++p)
	{

		mfxShadowTech->GetPassByIndex(p)->Apply(0, md3dImmediateContext);
		md3dImmediateContext->DrawIndexed(36, 0, 0);
	}

	ID3D11RenderTargetView* renderTargets[1] = { mRenderTargetView };
	md3dImmediateContext->OMSetRenderTargets(1, renderTargets, mDepthStencilView);
	md3dImmediateContext->RSSetViewports(1, &mScreenViewport);
}

void ShadowsApp::OnMouseDown(WPARAM btnState, int x, int y)
{
	mLastMousePos.x = x;
	mLastMousePos.y = y;

	SetCapture(mhMainWnd);
}

void ShadowsApp::OnMouseUp(WPARAM btnState, int x, int y)
{
	ReleaseCapture();
}

void ShadowsApp::OnMouseMove(WPARAM btnState, int x, int y)
{
	if( (btnState & MK_LBUTTON) != 0 )
	{
		// Make each pixel correspond to a quarter of a degree.
		float dx = XMConvertToRadians(0.25f*static_cast<float>(x - mLastMousePos.x));
		float dy = XMConvertToRadians(0.25f*static_cast<float>(y - mLastMousePos.y));

		// Update angles based on input to orbit camera around box.
		mTheta += dx;
		mPhi   += dy;

		// Restrict the angle mPhi.
		mPhi = MathHelper::Clamp(mPhi, 0.1f, MathHelper::Pi-0.1f);
	}
	else if( (btnState & MK_RBUTTON) != 0 )
	{
		// Make each pixel correspond to 0.005 unit in the scene.
		float dx = 0.005f*static_cast<float>(x - mLastMousePos.x);
		float dy = 0.005f*static_cast<float>(y - mLastMousePos.y);

		// Update the camera radius based on input.
		mRadius += dx - dy;

		// Restrict the radius.
		mRadius = MathHelper::Clamp(mRadius, 3.0f, 15.0f);
	}

	mLastMousePos.x = x;
	mLastMousePos.y = y;
}

void ShadowsApp::BuildGeometryBuffers() {
	BuildBoxGeometryBuffer();
	BuildPlaneGeometryBuffer();
}

void ShadowsApp::BuildBoxGeometryBuffer()
{
	// Create vertex buffer
    Vertex vertices[] =
    {
		//top
		{ XMFLOAT3(-1.0f, +1.0f, -1.0f), XMFLOAT3(+0.0f, +1.0f, +0.0f) },
		{ XMFLOAT3(-1.0f, +1.0f, +1.0f), XMFLOAT3(+0.0f, +1.0f, +0.0f) },
		{ XMFLOAT3(+1.0f, +1.0f, -1.0f), XMFLOAT3(+0.0f, +1.0f, +0.0f) },
		{ XMFLOAT3(+1.0f, +1.0f, +1.0f), XMFLOAT3(+0.0f, +1.0f, +0.0f) },

		//botom
		{ XMFLOAT3(-1.0f, +1.0f, -1.0f), XMFLOAT3(+0.0f, -1.0f, +0.0f) },
		{ XMFLOAT3(-1.0f, +1.0f, +1.0f), XMFLOAT3(+0.0f, -1.0f, +0.0f) },
		{ XMFLOAT3(+1.0f, +1.0f, -1.0f), XMFLOAT3(+0.0f, -1.0f, +0.0f) },
		{ XMFLOAT3(+1.0f, +1.0f, +1.0f), XMFLOAT3(+0.0f, -1.0f, +0.0f) },

		//right
		{ XMFLOAT3(+1.0f, -1.0f, -1.0f), XMFLOAT3(+1.0f, +0.0f, +0.0f) },
		{ XMFLOAT3(+1.0f, -1.0f, +1.0f), XMFLOAT3(+1.0f, +0.0f, +0.0f) },
		{ XMFLOAT3(+1.0f, +1.0f, -1.0f), XMFLOAT3(+1.0f, +0.0f, +0.0f) },
		{ XMFLOAT3(+1.0f, +1.0f, +1.0f), XMFLOAT3(+1.0f, +0.0f, +0.0f) },

		//left
		{ XMFLOAT3(-1.0f, -1.0f, -1.0f), XMFLOAT3(-1.0f, +0.0f, +0.0f) },
		{ XMFLOAT3(-1.0f, -1.0f, +1.0f), XMFLOAT3(-1.0f, +0.0f, +0.0f) },
		{ XMFLOAT3(-1.0f, +1.0f, -1.0f), XMFLOAT3(-1.0f, +0.0f, +0.0f) },
		{ XMFLOAT3(-1.0f, +1.0f, +1.0f), XMFLOAT3(-1.0f, +0.0f, +0.0f) },

		//back
		{ XMFLOAT3(-1.0f, -1.0f, +1.0f), XMFLOAT3(+0.0f, +0.0f, +1.0f) },
		{ XMFLOAT3(+1.0f, -1.0f, +1.0f), XMFLOAT3(+0.0f, +0.0f, +1.0f) },
		{ XMFLOAT3(-1.0f, +1.0f, +1.0f), XMFLOAT3(+0.0f, +0.0f, +1.0f) },
		{ XMFLOAT3(+1.0f, +1.0f, +1.0f), XMFLOAT3(+0.0f, +0.0f, +1.0f) },

		//back
		{ XMFLOAT3(-1.0f, -1.0f, -1.0f), XMFLOAT3(+0.0f, +0.0f, -1.0f) },
		{ XMFLOAT3(+1.0f, -1.0f, -1.0f), XMFLOAT3(+0.0f, +0.0f, -1.0f) },
		{ XMFLOAT3(-1.0f, +1.0f, -1.0f), XMFLOAT3(+0.0f, +0.0f, -1.0f) },
		{ XMFLOAT3(+1.0f, +1.0f, -1.0f), XMFLOAT3(+0.0f, +0.0f, -1.0f) }
    };

    D3D11_BUFFER_DESC vbd;
    vbd.Usage = D3D11_USAGE_IMMUTABLE;
    vbd.ByteWidth = sizeof(Vertex) * 24;
    vbd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
    vbd.CPUAccessFlags = 0;
    vbd.MiscFlags = 0;
	vbd.StructureByteStride = 0;
    D3D11_SUBRESOURCE_DATA vinitData;
    vinitData.pSysMem = vertices;
    HR(md3dDevice->CreateBuffer(&vbd, &vinitData, &mBoxVB));


	// Create the index buffer

	UINT indices[] = {
		//top
		0, 1, 2,
		3, 2, 1,

		//bottom
		6, 5, 4,
		5, 6, 7,

		//right
		10, 9, 8,
		9, 10, 11,

		//left
		12, 13, 14,
		15, 14, 13,

		//back
		16, 17, 18,
		19, 18, 17,

		//front
		22, 21, 20,
		21, 22, 23

	};

	D3D11_BUFFER_DESC ibd;
    ibd.Usage = D3D11_USAGE_IMMUTABLE;
    ibd.ByteWidth = sizeof(UINT) * 36;
    ibd.BindFlags = D3D11_BIND_INDEX_BUFFER;
    ibd.CPUAccessFlags = 0;
    ibd.MiscFlags = 0;
	ibd.StructureByteStride = 0;
    D3D11_SUBRESOURCE_DATA iinitData;
    iinitData.pSysMem = indices;
    HR(md3dDevice->CreateBuffer(&ibd, &iinitData, &mBoxIB));
}

void ShadowsApp::BuildPlaneGeometryBuffer() {
	// Create vertex buffer
	Vertex vertices[] =
	{
		{ XMFLOAT3(-1.0f, +0.0f, -1.0f), XMFLOAT3(+0.0f, +1.0f, +0.0f) },
		{ XMFLOAT3(-1.0f, +0.0f, +1.0f), XMFLOAT3(+0.0f, +1.0f, +0.0f) },
		{ XMFLOAT3(+1.0f, +0.0f, -1.0f), XMFLOAT3(+0.0f, +1.0f, +0.0f) },
		{ XMFLOAT3(+1.0f, +0.0f, +1.0f), XMFLOAT3(+0.0f, +1.0f, +0.0f) }
	};

	D3D11_BUFFER_DESC vbd;
	vbd.Usage = D3D11_USAGE_IMMUTABLE;
	vbd.ByteWidth = sizeof(Vertex)* 4;
	vbd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vbd.CPUAccessFlags = 0;
	vbd.MiscFlags = 0;
	vbd.StructureByteStride = 0;
	D3D11_SUBRESOURCE_DATA vinitData;
	vinitData.pSysMem = vertices;
	HR(md3dDevice->CreateBuffer(&vbd, &vinitData, &mPlaneVB));


	// Create the index buffer

	UINT indices[] = {
		0, 1, 2,
		2, 1, 3
	};

	D3D11_BUFFER_DESC ibd;
	ibd.Usage = D3D11_USAGE_IMMUTABLE;
	ibd.ByteWidth = sizeof(UINT)* 6;
	ibd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	ibd.CPUAccessFlags = 0;
	ibd.MiscFlags = 0;
	ibd.StructureByteStride = 0;
	D3D11_SUBRESOURCE_DATA iinitData;
	iinitData.pSysMem = indices;
	HR(md3dDevice->CreateBuffer(&ibd, &iinitData, &mPlaneIB));
}

void ShadowsApp::BuildFXShadow()
{
	DWORD shaderFlags = 0;
#if defined( DEBUG ) || defined( _DEBUG )
	shaderFlags |= D3D10_SHADER_DEBUG;
	shaderFlags |= D3D10_SHADER_SKIP_OPTIMIZATION;
#endif

	ID3D10Blob* compiledShader = 0;
	ID3D10Blob* compilationMsgs = 0;
	HRESULT hr = D3DX11CompileFromFile(L"FX/BuildShadowMap.fx", 0, 0, 0, "fx_5_0", shaderFlags,
		0, 0, &compiledShader, &compilationMsgs, 0);

	if (!compiledShader) {
		throw EXCEPTION_BREAKPOINT;
	}

	// compilationMsgs can store errors or warnings.
	if (compilationMsgs != 0)
	{
		MessageBoxA(0, (char*)compilationMsgs->GetBufferPointer(), 0, 0);
		ReleaseCOM(compilationMsgs);
	}

	// Even if there are no compilationMsgs, check to make sure there were no other errors.
	if (FAILED(hr))
	{
		DXTrace(__FILE__, (DWORD)__LINE__, hr, L"D3DX11CompileFromFile", true);
	}

	HR(D3DX11CreateEffectFromMemory(compiledShader->GetBufferPointer(), compiledShader->GetBufferSize(),
		0, md3dDevice, &mFXShadow));

	// Done with compiled shader.
	ReleaseCOM(compiledShader);

	mfxShadowTech = mFXShadow->GetTechniqueByName("ShadowMapTech");
	mfxShadowWorldViewProj = mFXShadow->GetVariableByName("gWorldViewProj")->AsMatrix();
	mfxShadowHeightScale = mFXShadow->GetVariableByName("gHeightScale")->AsScalar();
}

 
void ShadowsApp::BuildFX()
{
	DWORD shaderFlags = 0;
#if defined( DEBUG ) || defined( _DEBUG )
    shaderFlags |= D3D10_SHADER_DEBUG;
	shaderFlags |= D3D10_SHADER_SKIP_OPTIMIZATION;
#endif
 
	ID3D10Blob* compiledShader = 0;
	ID3D10Blob* compilationMsgs = 0;
	HRESULT hr = D3DX11CompileFromFile(L"FX/basic.fx", 0, 0, 0, "fx_5_0", shaderFlags, 
		0, 0, &compiledShader, &compilationMsgs, 0);

	if (!compiledShader) {
		throw EXCEPTION_BREAKPOINT;
	}

	// compilationMsgs can store errors or warnings.
	if( compilationMsgs != 0 )
	{
		MessageBoxA(0, (char*)compilationMsgs->GetBufferPointer(), 0, 0);
		ReleaseCOM(compilationMsgs);
	}

	// Even if there are no compilationMsgs, check to make sure there were no other errors.
	if(FAILED(hr))
	{
		DXTrace(__FILE__, (DWORD)__LINE__, hr, L"D3DX11CompileFromFile", true);
	}

	HR(D3DX11CreateEffectFromMemory(compiledShader->GetBufferPointer(), compiledShader->GetBufferSize(), 
		0, md3dDevice, &mFX));

	// Done with compiled shader.
	ReleaseCOM(compiledShader);

	mTech = mFX->GetTechniqueByName("Light1Tech");
	mfxWorld = mFX->GetVariableByName("gWorld")->AsMatrix();
	mfxWorldViewProj = mFX->GetVariableByName("gWorldViewProj")->AsMatrix();
	mfxWorldInvTranspose = mFX->GetVariableByName("gWorldInvTranspose")->AsMatrix();
	mfxEyePos = mFX->GetVariableByName("gEyePosW")->AsVector();
	mfxDirLights = mFX->GetVariableByName("gDirLights");
	mfxMaterial = mFX->GetVariableByName("gMaterial");
	mfxShadowMap = mFX->GetVariableByName("gShadowMap")->AsShaderResource();
	mfxShadowTransform = mFX->GetVariableByName("gShadowTransform")->AsMatrix();
}

void ShadowsApp::BuildVertexLayout()
{
	// Create the vertex input layout.
	D3D11_INPUT_ELEMENT_DESC vertexDesc[] =
	{
		{"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0},
		{"NORMAL",    0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0}
	};

	// Create the input layout
    D3DX11_PASS_DESC passDesc;
    mTech->GetPassByIndex(0)->GetDesc(&passDesc);
	HR(md3dDevice->CreateInputLayout(vertexDesc, 2, passDesc.pIAInputSignature, 
		passDesc.IAInputSignatureSize, &mInputLayout));
}

void ShadowsApp::BuildShadowTransform()
{
	// Only the first "main" light casts a shadow.
	XMVECTOR lightDir = XMLoadFloat3(&mDirLights[0].Direction);
	XMVECTOR lightPos = -2.0f*mSceneBounds.Radius*lightDir;
	XMVECTOR targetPos = XMLoadFloat3(&mSceneBounds.Center);
	XMVECTOR up = XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);

	XMMATRIX V = XMMatrixLookAtLH(lightPos, targetPos, up);

	// Transform bounding sphere to light space.
	XMFLOAT3 sphereCenterLS;
	XMStoreFloat3(&sphereCenterLS, XMVector3TransformCoord(targetPos, V));

	// Ortho frustum in light space encloses scene.
	float l = sphereCenterLS.x - mSceneBounds.Radius;
	float b = sphereCenterLS.y - mSceneBounds.Radius;
	float n = sphereCenterLS.z - mSceneBounds.Radius;
	float r = sphereCenterLS.x + mSceneBounds.Radius;
	float t = sphereCenterLS.y + mSceneBounds.Radius;
	float f = sphereCenterLS.z + mSceneBounds.Radius;
	XMMATRIX P = XMMatrixOrthographicOffCenterLH(l, r, b, t, n, f);

	// Transform NDC space [-1,+1]^2 to texture space [0,1]^2
	XMMATRIX T(
		0.5f, 0.0f, 0.0f, 0.0f,
		0.0f, -0.5f, 0.0f, 0.0f,
		0.0f, 0.0f, 1.0f, 0.0f,
		0.5f, 0.5f, 0.0f, 1.0f);

	XMMATRIX S = V*P*T;

	XMStoreFloat4x4(&mLightView, V);
	XMStoreFloat4x4(&mLightProj, P);
	XMStoreFloat4x4(&mShadowTransform, S);
}